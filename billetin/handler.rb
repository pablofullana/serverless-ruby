require 'typhoeus'

def handler event={}
  response = Typhoeus.get('www.bna.com.ar', followlocation: true)

  currency, ask, bid = response
    .body
    .scan(/<td class="tit">Dolar U.S.A[^r]*/)
    .first
    .gsub(/(<[^>]*>)|\n|\t/s) {" "}
    .split("\r")
    .first(3)
    .each(&:strip!)

  updated_at = response
    .body
    .scan(/Hora Actualizaci[^<]*/)
    .first
    .split
    .last
    .concat(' ART')

  return {
    currency: currency,
    ask: ask,
    bid: bid,
    source: 'BNA',
    updated_at: updated_at
  }
end
